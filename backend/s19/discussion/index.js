//Syntax, statements, and comments

//For us to create a comment, we use ctrl + /

console.log("We tell the computer to log in this console.");
alert("We tell the computer to display an alert with this message!");

//Statements - in programming are instructions that we tell the computer to perform
//JS statements usually end with ;

//Syntax  - it is the set of rules that describes how statements must be constructed



//Comments
//For us to create a comment, we use Ctrl + /
//We have a single line (ctrl + /) and multiple-line comments (ctrl + shift + /)

//console.log("Hello!")

//alert("This is an alert!")
//alert("This is another alert");

//Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS.

//Variables 
//This is used to contain data

//Declare variables
	//We tell our devices that a variableName is created and is ready to store data
	//If we do not give a name yet, we get "undefined"
	//Syntax
		//let/const variableName;

		let myVariable;
		console.log(myVariable);

		let hello;
		//if we do not declare ("hello" above), we get an error
		//variables must be declared first before they are used
		console.log(hello);

		//declaring and initializing variables
		//syntax
			//let/const variableName = initialValue;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;

		//re-assigning variable values 

		productName = 'Laptop';
		console.log(productName);

		let friend = 'Kate';
		friend = 'Jane'


		///////////////////////

		//Declare a variable
		let supplier;

		//Initialization is done after the variable has been declared
		supplier = "John Smith Trading";

		//Re-assignment because the its initial value was already declared
		supplier = "Zuitt Store";


		//We cannot declare a const variable without initialization
		const pi = 3.1416;
		console.log(pi);

		//Multiple variable decalrations
		let productCode = 'DC017' , productBrand = 'Dell';


		//let productCode = 'DC017';
		//const productBrand = 'Dell';
		console.log(productCode, productBrand);

		//Date Types

		//Strings
		//Strings are a series of characters that create a word, a phrase, or a sentence or anything related to creating a text

		let country = 'Philippines';
		let province = 'Metro Manila';

		//Concatenating Strings

		let fullAddress = province + ' , ' + country;
		console.log(fullAddress)

		let greeting = 'I live in the ' + country;
		console.log(greeting);

		//the escape character (\) in strings in combination  with other characters then produce different effects

		//"\n" refers to creating a new line in between text

		let mailAddress = 'Metro Manila\n\n\n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);
		message = 'John\'s employees went home early';
		console.log(message)


		//Numbers

		let headcount = 26;
		console.log(headcount);
		let grade = 98.7; 
		console.log(grade);
		let planetDistance = 2e10;
		console.log(planetDistance);

		//Combine text and strings
		console.log("John's first grade last quarter is " + grade);

		//Boolean 
		let isMarried = false;
		let isGoodConduct = true;
		console.log(isMarried);

		console.log("isGoodConduct: " + isGoodConduct);

		//Arrays
		let grades = [98.7,92.1,90.7,98.6];
		console.log(grades);

		//Objects

		let MyGrades = {
			firstGrading: 98.7,
			secondGrading: 93.1,
			thirdGrading: 90.7,
			fourthGrading: 94.6,
		}
		console.log(MyGrades)

		let person = {

			fullName: 'Juan Dela Cruz',
			Age: 35,
			isMarried: false,
			Contact: ["=631980761816","4500"],
			Address: {
				houseNumber: '345',
				city: 'Manila'
			}

		}
		console.log(person);


		//typeof operator

		console.log(typeof person)



//null
	//it is used to intentionally express the absence of the value in a variable declaration/initialization

	let spouse = null;

//undefined
	//this represents the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);//undefined
