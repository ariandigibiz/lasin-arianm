console.log("Hello World")

//Arithmetic Operators
//+,-,*,/,%

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	//difference 

	let a = 1397;
	let b = 7831;

	let difference = a - b;
	console.log("Result of subtraction operator: " + difference);

	//product

	let c = 1397;
	let d = 7831;

	let product = c * d;
	console.log("Result of multiplication operator: " + product);

	//quotient
	let e = 1397;
	let f = 7831;

	let quotient = e / f;
	console.log("Result of division operator: " + quotient);

	//remainder
	let g = 1397;
	let h = 7831;

	let remainder = g % h;
	console.log("Result of modulo operator: " + remainder);


	//another way


	let i = 10;
	let j= 5;

	let remainderA = i % j;
	console.log(remainderA);//0

//Assignment Operator
//Basic Assignment operator (=)

let assignmentNumber = 8; 

//Addition Assignment Operator

assignmentNumber = assignmentNumber + 2;//long method
assignmentNumber += 2;//short method
console.log(assignmentNumber);

//Subtraction/Multiplication/Division Assignment Operator
//calculate simultaneously not individually

assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of *= : " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of /= : " + assignmentNumber);

//Multiple Operators and ()

//

	let mdas = 1 + 2 - 3 * 4 /5;
	console.log(mdas);

//

	let pemdas = 1 + (2 - 3) * (4/5);
	console.log(pemdas);


//Increment and Decrement
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z;

//the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//the value of z is returned and stored in the variable "increment" then the value of z is increased by one

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

//decrement

let decrement = --z

decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//Type Coercion
	//Type Coercion is the automatic or implicit conversion of values from one data type to another


let numA ='10';
let numB =12;

let coercion = numA + numB;
console.log(coercion);//1012
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

//Comparison Operators

let juan = 'juan';

//Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);

//Inequality Operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

//Strict checks not just value but also data type

//Strict Equality Operator (===)

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

//Strict Inequality Operator (!==)

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

//Relational Operators

let abc = 50;
let def = 65;

let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);


let numStr = "30";
console.log(abc > numStr);
console.log(def <= numStr);

let str ='twenty';
console.log(def >= str);