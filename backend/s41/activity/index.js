const express = require("express");
const mongoose =require('mongoose');

const app = express();
const port = 3001;




app.use(express.json());

app.use(express.urlencoded({extended: true}));



mongoose.connect("mongodb+srv://ariandigibiz:tYp1WQgzYOVAnqys@batch-297.habm4ik.mongodb.net/taskDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});



let db = mongoose.connection;



db.on("error",console.error.bind(console, "connection error"));


db.once("open", ()=>console.log("We're connected to the cloud database"))



const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
    if (req.body.username === '' || req.body.password === '') {
        return res.send("BOTH username and password must be provided.");
    }

    User.findOne({ username: req.body.username }).then((result, err) => {
        if (err) {
            return console.error(err);
        }

        if (result !== null && result.username === req.body.username && result.password === req.body.password) {
            return res.send("Duplicate username found");
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });

            newUser.save().then((saveUser, saveErr) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New user registered");
                }
            });
        }
    });
});















//Listen to the port, meaning, if the port is accessed, we will run the server
if(require.main === module){
	app.listen(port, ()=>console.log(`Server running at port ${port}`));
}

module.exports = app;