console.log("Hello World")

//Functions
	//Parameters and Arguments

/*	function printName(){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi " + nickname);
	}

	printName();*/

function printName(name){

	console.log("Hi, " + name);

}

printName("Yan");
//another way (aside from code above)

let sampleVariable = "Arianne";

printName(sampleVariable);

function checkDivisibiltyBy8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibiltyBy8(64);
checkDivisibiltyBy8(28);
checkDivisibiltyBy8(9678);

//Mini-activity

function checkDivisibiltyBy4(num){

	let remainder = num % 4;
	console.log("The remainder of " + num + " divided by 4 is: " + remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log("Is " + num + " divisible by 4?");
	console.log(isDivisibleBy4);
}

checkDivisibiltyBy4(56);
checkDivisibiltyBy4(95);
checkDivisibiltyBy4(444);



//Functions as arguments
//Function parameters can also accept other functions as arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");

};

function invokeFunction(argumentFunction){
	argumentFunction();
}	

//Adding and removing the parentheses  "()" impacts the output of JS heavily 
//when a fucntion is used with parentheses "()", it denotes invoking/calling a function

//A function used without a parentheses is normally associated with using the function as an argument to another function

invokeFunction(argumentFunction);

//Using multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);

};

createFullName('Juan','Dela','Cruz')
createFullName('Cruz','Juan','Dela')
createFullName('Juan','Dela')//there will an empty parameter "undefined"
createFullName('Juan','Dela','Cruz','III')//4th parameter not included

//Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName,middleName,lastName);


/*

create a function called printFriends

3 parameters 
friend1, friend2, friend3

*/

function printFriends(friend1, friend2, friend3){
	console.log("My girls are: " + friend1 + ' , ' + friend2 + ' , ' + friend3);
}
	let friend1 = "Beyonce";
	let friend2 = "Iggy";
	let friend3 = "Shakira";



printFriends(friend1,friend2,friend3)

//answer

/*function printFriends(friend1, friend2, friend3){
	console.log("My girls are: " + friend1 + ' , ' + friend2 + ' , ' + friend3);
}

printFriends("Beyonce","Iggy","Shakira");

*/


//Return Statement

function returnFullName(firstName,middleName,lastName){

	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be printed!");


}

let completeName1 = returnFullName("Monkey","D","Luffy");
let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

console.log(completeName1 + " is my bestfriend!");
console.log(completeName2 + " is my friend!");


//Mini-activity

//area of a square

function getSquareArea(side){

	return side*side
}

let areaSquare = getSquareArea(4);

console.log("The are of a square is " + areaSquare);
console.log(areaSquare);

//add 3 numbers

function getSumOfNumbers(number1,number2,number3){

	return number1 + number2 + number3
}


let sumOfNumbers = getSumOfNumbers(1,2,3)
console.log("The sum of 3 numbers is: " + sumOfNumbers) 
console.log(sumOfNumbers)

//if number is equal to 100

function is6EqualTo100(num){

	return num === 100
}

let equalTo100 = is6EqualTo100(6);
console.log("Is 6 Equal to 100? " + equalTo100)
console.log(equalTo100)
