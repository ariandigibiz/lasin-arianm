console.log("Hi, B297!")

console.log("maybe")

function printline(){
	console.log("maybe")
};

printline();

//functions
//lines/blocks of code that tell our devices to perform a certain task when called/invoked

//Function declaration

//

	//Syntax:

		//function functionName() {
			//code block (statement)
		//}

//


//function declaration

function printName(){
	console.log("My Name is Ari");
};

//function Invocation
printName();

declaredFunction();

//Function declaration vs expressions

//Function Declaration
	//function declaration is created with the function keyword and adding a function name
//they are "saved for later use"

function declaredFunction(){
	console.log("Hello from declaredFunction")
}

declaredFunction();

//Function Expression
	//function expression is stored in a variable
	//function expression is an anonymous function assigned to the variable function

//variableFunction(); 
	//index.js:54 Uncaught ReferenceError: Cannot access 'variableFunction' before initialization


let variableFunction = function() {
	console.log("Hello from function expression")
};

variableFunction();

//a function expression of function named funcName assigned to the variable funcExpression

let funcExpression = function funcName() {
	console.log("Hello from the other side");
};

funcExpression();

//We can also reassign decalred functions and fucntion expressions to new anonymous functions

declaredFunction = function(){
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
};

funcExpression();

const constantFunc = function(){
	console.log("initialized with const!");

}

constantFunc();

//constantFunc = function(){
	//console.log("Cannot be reassigned!")
//}

//constantFunc();


/*{
	let a = 1;
}

let a = 1;

function sameple(){
	let a = 1;
}
*/

{
	let localVar = "Amarmando Perez";
}

let globalVar = "Mr. WorldWide"

//console.log(localVar);//result in an error
console.log(globalVar)


function showNames() {

	var functionVar = "Joe";
	const functionConst = "John"
	let functionLet = "Jane"

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

/*console.log(functionVar);
console.log(functionConst);
console.log(functionLet);
*/
//do not add OUTSIDE of function scope




//Nested Functions

function myNewFunction(){

	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(nestedName);
	}

	//console.log(nestedName);
		//error

	nestedFunction(); //always invoke inside function scope in order to show in console
}

myNewFunction();

//nestedFunction(); is declared inside the myNewFunction scope

//Function and Global Scoped Variables

//Global Scoped Variable

let globalName = "Cardo";

function myNewFunction2(){
	let nameInside = "Hillary";
	console.log(globalName)

}

myNewFunction2();
//console.log(nameInside) this results in an error. 


/*


//Using alert()

//alert("This will run immediately when the page loads")

function showSampleAlert(){
	alert("Hello, Earthlings! This is from a function!")
}

showSampleAlert(); 
	//this is an invocation. and alert under function will not run with this. 



console.log("I will only log in the console when the alert is dismissed")

		//see magic above. showSampleAlert will show first then next console will only show after alert is dismissed.



//Using prompt()

let samplePrompt = prompt("Enter your name");

console.log("Hi I am " + samplePrompt);
	//see magic 

//prompt returns an empty string when there is no input. or null if the user cancels the prompt()
//we can also use this to gatehr user info

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name ");
	let lastName = prompt("Enter your last name: ")

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}

printWelcomeMessage();

*/

//The Return Statement

	//The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(){
	return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	console.log("This message will not be printed!")
		//take note. follow below for it to show on console
		//return can return value
}

let fullName = returnFullName();
console.log(fullName);

function returnFullAddress(){
	let fullAddress = {

		street : "#123 st.",
		city : "san francisco",
		province: "California",
	}

	return fullAddress;
}

let myAddress = returnFullAddress();
	console.log(myAddress);

	function printPlayerInfo(){

		console.log("Username: " + "dark_magician");
		console.log("Level: " + 95);
		console.log("Job: " + "Mage");
	}

	let user1 = printPlayerInfo();
	console.log(user1)//undefined

	function returnSumOf5And10(){
		return 5 + 10;
	}

	let sumOf5And10 = returnSumOf5And10();//15
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5And10();//115
	console.log(total);

	//numbers are saved through return only. use return for numbers.

	function getGuildMembers(){

		return ["Lulu", "Tristana", "Teemo"];

	}

	console.log(getGuildMembers());


	//Fucntion Naming Conventions

	function getCourses(){
		let courses = ["ReactJs 101", "ExpressJs 101", "MongoDB"];
		return courses;
	}

	let courses = getCourses();
	console.log(courses);


	//avoid using generic names and pointless and inappropriate function names
	function get(){
		let color = "pink";
		return name;
	}

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	}

	displayCarInfo();

	function getUserInfo (){

		return {

			name: "John Doe",
			age: 25,
			address: "123",
			street: "sf cali",
			isMarried: false,
			petName: "Doey"
		}
	}

	let userInfo = getUserInfo();
	console.log(userInfo);

	